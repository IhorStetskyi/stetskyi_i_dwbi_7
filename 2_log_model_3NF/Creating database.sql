--just link --https://app.sqldbm.com/SQLServer/Edit/p26475/#

use [master]
go

if db_id('products') is not null
begin
	print 'deleting db products'
	alter database [products] set single_user with rollback immediate
	drop database products
end

create database products
go

use products
go


create table [description]
(
	id int primary key identity(1,1),
	product_description varchar(50),
	shelf_life_in_month int,
	image_id int,
	[type_id] int,
	boxing_id int
)
go

create table [image]
(
	id int primary key identity (1,1),
	product_image image
)
go

create table [type]
(
	id int primary key identity (1,1),
	product_type varchar(50)
)
go

create table [boxing]
(
	id int primary key identity (1,1),
	boxing_type varchar(50),
	amount_in_box int
)
go

create table [product]
(
	id int primary key identity (1,1),
	[name] varchar(50),
	unit_price money,
	description_id int
)
go


create table [director]
(
	id int primary key identity (1,1),
	[name] varchar(50),
	surname varchar(50)
)
go

create table [phone_number]
(
	id int primary key identity (1,1),
	phone varchar(50)
)
go

create table [provisioner]
(
	id int primary key identity (1,1),
	[name] varchar(50),
	adress_id int
)
go

create table [director_phone_number]
(
	id int primary key identity (1,1),
	director_id int,
	phone_number_id int
)
go

create table [provisioner_phone_number]
(
	id int primary key identity (1,1),
	provisioner_id int,
	phone_number_id int
)
go


create table [adress]
(
	id int primary key identity (1,1),
	provisioner_adress varchar(50)
)
go

create table [bill_of_landing]
(
	id int primary key identity (1,1),
	[date] datetime,
	provisioner_id int,
	product_id int,
	box_amount int,
	total_price money
)
go


--creating foreign keys

alter table provisioner
add constraint FK_provisoiner_adress_id foreign key (adress_id) references adress(id)
go

alter table director_phone_number
add constraint FK_director_phone_number_director_id foreign key (director_id) references director(id),
	constraint FK_director_phone_number_phone_number_id foreign key (phone_number_id) references phone_number(id)
go

alter table provisioner_phone_number
add constraint FK_provisioner_phone_number_provisioner_id foreign key (provisioner_id) references provisioner(id),
	constraint FK_provisioner_phone_number_phone_number_id foreign key (phone_number_id) references phone_number(id)
go

alter table bill_of_landing
add constraint FK_bill_of_landing_provisioner_id foreign key (provisioner_id) references provisioner(id),
	constraint FK_bill_of_landing_product_id foreign key (product_id) references product(id)
go

alter table product
add constraint FK_product_description_id foreign key (description_id) references [description](id)
go

alter table [description]
add constraint FK_description_type_id foreign key ([type_id]) references [type](id),
	constraint FK_description_boxing_id foreign key (boxing_id) references boxing(id)
go

--creating stored procedure
if OBJECT_ID('Total') is not null
begin
print 'deleting stored procedure Total'
drop procedure Total
end
go

CREATE PROCEDURE Total @pr int, @am int
AS
begin
declare @result money
SET NOCOUNT ON;
set @result =(
select ((amount_in_box * @am)*product.unit_price) from product
	inner join [description] on [description].id = product.[description_id]
	inner join boxing on boxing.id = [description].boxing_id
	where product.id = @pr)
	return @result
end
go


--creating triggers
if (OBJECT_ID('Trigger_bill') is not null)
begin
	print'deleting Trigger_bill'
	drop trigger Trigger_bill
end
go
CREATE TRIGGER Trigger_bill
	ON bill_of_landing
	for insert
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in insert'
		else
		begin
			DECLARE @id int;
			DECLARE @amount int;
			declare @BOLid int
			declare @res money;
			DECLARE cur CURSOR FAST_FORWARD LOCAL FOR
			SELECT id, product_id, box_amount
			FROM INSERTED;
			OPEN cur;
			 FETCH NEXT FROM cur INTO @BOLid, @id, @amount

			WHILE @@FETCH_STATUS=0
				BEGIN
					EXEC @res = Total @id, @amount
					update bill_of_landing set total_price = @res where bill_of_landing.id = @BOLid
					FETCH NEXT FROM cur	INTO @BOLid, @id, @amount
				END
			CLOSE cur;
			DEALLOCATE cur;
		end
	end
GO

if (OBJECT_ID('Forbidding_update') is not null)
begin
	print'deleting Forbidding_update'
	drop trigger Forbidding_update
end
go
create trigger Forbidding_update
on bill_of_landing
instead of update
as
begin
IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in insert'
		else
			print 'you can not update bill_of_landing'
			rollback
end
go





--inserting some data
insert into adress (provisioner_adress) values ('Naukova 145'), ('Chornovola 45')
insert into boxing (boxing_type, amount_in_box) values ('Kilograms', 6), ('Bottles', 8)
insert into [type] (product_type) values ('Food'), ('Alcohol')
insert into [description] (product_description, shelf_life_in_month, [type_id], [boxing_id]) values ('Great Potatos', 1, 1, 1), ('Best beer ever', 9, 2, 2)
insert into director ([name], surname) values ('Valentyn', 'Usbekov')
insert into phone_number (phone) values ('8-800-000-15-48'), ('8-800-555-35-35')
insert into director_phone_number (director_id, phone_number_id) values (1,1)
insert into product ([name], unit_price, description_id) values ('Potatos', 30.75, 1), ('Lvivske', 25.60, 2)
insert into provisioner ([name], [adress_id]) values ('LvivPosta4', 1)
insert into provisioner_phone_number (provisioner_id, phone_number_id) values (1,1)
insert into bill_of_landing ([date], provisioner_id, product_id, box_amount) values (GETDATE(),1,1,2), (GETDATE(),1,2,1)

