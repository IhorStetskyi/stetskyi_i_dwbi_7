
--creating triggers======================================================================================triggers
print 'Creating Triggers!!!!!!!!!!!'


--=========================================================================================================client
if (OBJECT_ID('Trigger_client') is not null)
begin
	print'deleting Trigger_client'
	drop trigger Trigger_client
end
go
CREATE TRIGGER Trigger_client
	ON client
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update client set updated_date = GETDATE()
	end
GO
--==========================================================================================================adress
if (OBJECT_ID('Trigger_adress') is not null)
begin
	print'deleting Trigger_adress'
	drop trigger Trigger_adress
end
go
CREATE TRIGGER Trigger_adress
	ON adress
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update adress set updated_date = GETDATE()
	end
GO
--============================================================================================avaiable_beer_in_bar
if (OBJECT_ID('Trigger_avaiable_beer_in_bar') is not null)
begin
	print'deleting Trigger_avaiable_beer_in_bar'
	drop trigger Trigger_avaiable_beer_in_bar
end
go
CREATE TRIGGER Trigger_avaiable_beer_in_bar
	ON avaiable_beer_in_bar
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update avaiable_beer_in_bar set updated_date = GETDATE()
	end
GO
--=============================================================================================================bar
if (OBJECT_ID('Trigger_bar') is not null)
begin
	print'deleting Trigger_bar'
	drop trigger Trigger_bar
end
go
CREATE TRIGGER Trigger_bar
	ON bar
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update bar set updated_date = GETDATE()
	end
GO
--======================================================================================================bar_clients
if (OBJECT_ID('Trigger_bar_clients') is not null)
begin
	print'deleting Trigger_bar_clients'
	drop trigger Trigger_bar_clients
end
go
CREATE TRIGGER Trigger_bar_clients
	ON bar_clients
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update bar_clients set updated_date = GETDATE()
	end
GO
--=============================================================================================================beer
if (OBJECT_ID('Trigger_beer') is not null)
begin
	print'deleting Trigger_beer'
	drop trigger Trigger_beer
end
go
CREATE TRIGGER Trigger_beer
	ON beer
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update beer set updated_date = GETDATE()
	end
GO
--==============================================================================================================city
if (OBJECT_ID('Trigger_city') is not null)
begin
	print'deleting Trigger_city'
	drop trigger Trigger_city
end
go
CREATE TRIGGER Trigger_city
	ON city
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update city set updated_date = GETDATE()
	end
GO
--===========================================================================================================country
if (OBJECT_ID('Trigger_country') is not null)
begin
	print'deleting Trigger_country'
	drop trigger Trigger_country
end
go
CREATE TRIGGER Trigger_country
	ON country
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update country set updated_date = GETDATE()
	end
GO
--============================================================================================================orders
if (OBJECT_ID('Trigger_orders') is not null)
begin
	print'deleting Trigger_orders'
	drop trigger Trigger_orders
end
go
CREATE TRIGGER Trigger_orders
	ON orders
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update orders set updated_date = GETDATE()
	end
GO
--==========================================================================================================provider
if (OBJECT_ID('Trigger_provider') is not null)
begin
	print'deleting Trigger_provider'
	drop trigger Trigger_provider
end
go
CREATE TRIGGER Trigger_provider
	ON provider
	FOR UPDATE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows in update'
		else
			update provider set updated_date = GETDATE()
	end
GO