--creating database education
USE master;
--deleting Education database=================================================================================database
if DB_ID('Education') is not null
begin
print 'deleting database Education'
ALTER DATABASE [Education] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
drop database [Education]
end
go
--creating Education database================================================================================
if DB_ID('Education') is null
begin
print 'creating database Education'
create database [Education]
end
go

use Education
go
create synonym dbo.education_client for I_S_module_3.dbo.client
create synonym dbo.education_adress for I_S_module_3.dbo.adress
create synonym dbo.education_avaiable_beer_in_bar for I_S_module_3.dbo.avaiable_beer_in_bar
create synonym dbo.education_bar for I_S_module_3.dbo.bar
create synonym dbo.education_bar_clients for I_S_module_3.dbo.bar_clients
create synonym dbo.education_beer for I_S_module_3.dbo.beer
create synonym dbo.education_city for I_S_module_3.dbo.city
create synonym dbo.education_country for I_S_module_3.dbo.country
create synonym dbo.education_gender for I_S_module_3.dbo.gender
create synonym dbo.education_orders for I_S_module_3.dbo.orders
create synonym dbo.education_view_bar_adress for I_S_module_3.dbo.view_bar_adress
create synonym dbo.education_view_beer for I_S_module_3.dbo.view_beer
create synonym dbo.education_view_beer_in_bar for I_S_module_3.dbo.view_beer_in_bar
create synonym dbo.education_view_client_info for I_S_module_3.dbo.view_client_info
create synonym dbo.education_view_orders_in_bar for I_S_module_3.dbo.view_orders_in_bar
go

select * from education_view_client_info
select * from education_client