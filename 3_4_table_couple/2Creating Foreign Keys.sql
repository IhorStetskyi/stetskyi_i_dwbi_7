--creating foreign keys
--================================================================================================================FK_client
begin
alter table client
ADD constraint FK_client_gender FOREIGN KEY (gender_id) REFERENCES gender(id),
    constraint FK_client_adress FOREIGN KEY (adress_id) REFERENCES adress(id),
	constraint FK_client_favoritebar FOREIGN KEY (favorite_bar_id) REFERENCES bar(id),
	constraint FK_client_favoritebeer FOREIGN KEY (favorite_beer_id) REFERENCES beer(id)
print 'created FK for table client'
end
go
--============================================================================================================FK_bar_clients
begin
alter table bar_clients
ADD constraint FK_barclients_bar FOREIGN KEY (bar_id) REFERENCES bar(id),
    constraint FK_barclients_client FOREIGN KEY (client_id) REFERENCES client(id)
print 'created FK for table bar_clients'
end
go
--===============================================================================================================FK_orders
begin
alter table orders
ADD constraint FK_orders_client FOREIGN KEY (client_id) REFERENCES client(id),
    constraint FK_orders_bar FOREIGN KEY (bar_id) REFERENCES bar(id),
    constraint FK_orders_beer FOREIGN KEY (beer_id) REFERENCES beer(id)
print 'created FK for table orders'
end
go
--====================================================================================================================FK_bar
begin
alter table bar
ADD constraint FK_bar_adress FOREIGN KEY (adress_id) REFERENCES adress(id)
print 'created FK for table bar'
end
go
--====================================================================================================FK_avaiable_beer_in_bar
begin
alter table avaiable_beer_in_bar
ADD constraint FK_avaiablebeerinbar_beer FOREIGN KEY (beer_id) REFERENCES beer(id),
    constraint FK_avaiablebeerinbar_bar FOREIGN KEY (bar_id) REFERENCES bar(id)
print 'created FK for table avaiable_beer_in_bar'
end
go
--===================================================================================================================FK_beer
begin
alter table beer
ADD constraint FK_beer_provider FOREIGN KEY (provider_id) REFERENCES provider(id)
print 'created FK for table beer'
end
go
--=================================================================================================================FK_adress
begin
alter table adress
ADD constraint FK_adress_city FOREIGN KEY (city_id) REFERENCES city(id)
print 'created FK for table adress'
end
go
--================================================================================================================FK_provider
begin
alter table provider
ADD constraint FK_provider_adress FOREIGN KEY (adress_id) REFERENCES adress(id)
print 'created FK for table provider'
end
go
--====================================================================================================================FK_city
begin
alter table city
ADD constraint FK_city_country FOREIGN KEY (country_id) REFERENCES country(id)
print 'created FK for table city'
end
go
