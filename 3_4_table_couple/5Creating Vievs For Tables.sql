
--Creating vievs for tables============================================================================vievs for tables
print 'creating views'
--==========================================================================================================view_client_info
if (OBJECT_ID('view_client_info') is not null)
begin
	print'deleting view_client_info'
	drop view view_client_info
end
go

CREATE VIEW view_client_info 
AS
SELECT 
	ROW_NUMBER() OVER( ORDER BY client.name) AS id,
	client.name AS client_name, 
	surname AS client_surname,
	age AS age,
	gender.name AS gender
FROM client
inner join gender on gender.id = client.gender_id
go

--==========================================================================================================view_bar_adress
if (OBJECT_ID('view_bar_adress') is not null)
begin
	print'deleting view_bar_adress'
	drop view view_bar_adress
end
go

CREATE VIEW view_bar_adress 
AS
SELECT 
    ROW_NUMBER() OVER( ORDER BY bar.name) AS id,
	bar.name as bar_name,
	adress.name AS street, 
	city.name AS city,
	country.name as country
FROM bar
inner join adress on bar.adress_id = adress.id
inner join city on city.id = adress.city_id
inner join country on city.country_id = country.id
go

--==========================================================================================================view_orders_in_bar
if (OBJECT_ID('view_orders_in_bar') is not null)
begin
	print'deleting view_orders_in_bar'
	drop view view_orders_in_bar
end
go

CREATE VIEW view_orders_in_bar 
AS
SELECT 
    ROW_NUMBER() OVER( ORDER BY bar.name) AS id,
	bar.name as bar_name,
	client.name AS client_name, 
	client.surname AS client_surname,
	beer.name as beer,
	amount as amount
FROM orders
inner join client on orders.client_id = client.id
inner join bar on orders.bar_id = bar.id
inner join beer on orders.beer_id = beer.id
go

--==========================================================================================================view_beer_in_bar
if (OBJECT_ID('view_beer_in_bar') is not null)
begin
	print'deleting view_beer_in_bar'
	drop view view_beer_in_bar
end
go

CREATE VIEW view_beer_in_bar 
AS
SELECT 
    ROW_NUMBER() OVER( ORDER BY bar.name) AS id,
	bar.name as bar,
	beer.name AS avaiable_beer
FROM avaiable_beer_in_bar
inner join bar on avaiable_beer_in_bar.bar_id = bar.id
inner join beer on avaiable_beer_in_bar.beer_id = beer.id
go

--==========================================================================================================view_beer_in_bar
if (OBJECT_ID('view_beer') is not null)
begin
	print'deleting view_beer'
	drop view view_beer
end
go

CREATE VIEW view_beer 
AS
SELECT 
    ROW_NUMBER() OVER( ORDER BY beer.name) AS id,
	name,
    volume,
    price
FROM beer
where price is not null
with check option
go




--=========================================================================================================selects from views
select * from view_client_info
select * from view_bar_adress
select * from view_orders_in_bar
select * from view_beer_in_bar
select * from view_beer

--insert into view_beer (name, volume) values ('Chernigivske', 0.5) - ������� ����� check option
--insert into view_beer (name, volume, price) values ('Chernigivske', 0.5, 25) - �����

