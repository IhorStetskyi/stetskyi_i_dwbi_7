USE master;
--deleting I_S_module_3 database=================================================================================database
if DB_ID('I_S_module_3') is not null
begin
print 'deleting database I_S_module_3'
ALTER DATABASE [I_S_module_3] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
drop database [I_S_module_3]
end
go
--creating I_S_module_3 database
if DB_ID('I_S_module_3') is null
begin
print 'creating database I_S_module_3'
create database [I_S_module_3]
end
go


use I_S_module_3
go

--deleting table ================================================================================================client
if (OBJECT_ID('client') is not null)
begin
print'deleting table client'
drop table client
end
go
--creating table client
if (OBJECT_ID('client') is null)
begin
print 'creating table client'
create table client
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	surname varchar(50),
	age int constraint MustBeAtLeast18YearsOld check (age >= 18),
	client_weight float,
	birth_date date,
	registration_date datetime,
	adress_id int,
	gender_id int not null,
	favorite_bar_id int,
	favorite_beer_id int,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueNameSurname Unique(name, surname)
)
end
go

--deleting table ==================================================================================================gender
if (OBJECT_ID('gender') is not null)
begin
print'deleting table gender'
drop table gender
end
go
--creating table gender and inserting values
if (OBJECT_ID('gender') is null)
begin
print 'creating table gender'
create table gender
(
	id int primary key identity(0,1),
	name varchar(50) not null,
)
end
go


--deleting table =============================================================================================bar_clients
if (OBJECT_ID('bar_clients') is not null)
begin
print'deleting table bar_clients'
drop table bar_clients
end
go
--creating table bar_clients
if (OBJECT_ID('bar_clients') is null)
begin
print 'creating table bar_clients'
create table bar_clients
(
	id int primary key identity(0,1),
	bar_id int not null,
	client_id int not null,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueBarClient Unique(bar_id, client_id)
)
end
go

--deleting table ===================================================================================================bar
if (OBJECT_ID('bar') is not null)
begin
print'deleting table bar'
drop table bar
end
go
--creating table bar
if (OBJECT_ID('bar') is null)
begin
print 'creating table bar'
create table bar
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	foundation_date date not null,
	capacity int not null,
	bar_square int not null,
	adress_id int not null,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueNameAdressOfBar Unique(name, adress_id)
)
end
go

--deleting table =====================================================================================avaiable_beer_in_bar
if (OBJECT_ID('avaiable_beer_in_bar') is not null)
begin
print'deleting table avaiable_beer_in_bar'
drop table avaiable_beer_in_bar
end
go
--creating table avaiable_beer_in_bar
if (OBJECT_ID('avaiable_beer_in_bar') is null)
begin
print 'creating table avaiable_beer_in_bar'
create table avaiable_beer_in_bar
(
	id int primary key identity(0,1),
	beer_id int not null,
	bar_id int not null,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueBeerBar Unique(beer_id, bar_id)
)
end
go

--deleting table ====================================================================================================beer
if (OBJECT_ID('beer') is not null)
begin
print'deleting table beer'
drop table beer
end
go
--creating table beer
if (OBJECT_ID('beer') is null)
begin
print 'creating table beer'
create table beer
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	volume float not null,
	price smallmoney,
	provider_id int,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueNameVolumePriceProvider Unique(name, volume, price, provider_id)
)
end
go

--deleting table ==================================================================================================adress
if (OBJECT_ID('adress') is not null)
begin
print'deleting table adress'
drop table adress
end
go
--creating table adress
if (OBJECT_ID('adress') is null)
begin
print 'creating table adress'
create table adress
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	city_id int not null,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueAdressNameAndCity Unique(name, city_id)
)
end
go

--deleting table ================================================================================================provider
if (OBJECT_ID('provider') is not null)
begin
print'deleting table provider'
drop table provider
end
go
--creating table provider
if (OBJECT_ID('provider') is null)
begin
print 'creating table provider'
create table provider
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	adress_id int,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueNameAdressOfProvider Unique(name, adress_id)
)
end
go

--deleting table ===================================================================================================city
if (OBJECT_ID('city') is not null)
begin
print'deleting table city'
drop table city
end
go
--creating table city
if (OBJECT_ID('city') is null)
begin
print 'creating table city'
create table city
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	city_population int,
	city_square float,
	foundation_date date,
	country_id int not null,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueNameCountry Unique(name, country_id)
)
end
go

--deleting table ================================================================================================country
if (OBJECT_ID('country') is not null)
begin
print'deleting table country'
drop table country
end
go
--creating table country
if (OBJECT_ID('country') is null)
begin
print 'creating table country'
create table country
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	country_population int,
	country_square float,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueCountry Unique(name)
)
end
go

--deleting table =================================================================================================orders
if (OBJECT_ID('orders') is not null)
begin
print'deleting table orders'
drop table orders
end
go
--creating table orders
if (OBJECT_ID('orders') is null)
begin
print 'creating table orders'
create table orders
(
	id int primary key identity(0,1),
	client_id int not null,
	bar_id int not null,
	beer_id int not null,
	amount int not null,
	inserted_date datetime default getdate(),
	updated_date datetime
)
end
go
