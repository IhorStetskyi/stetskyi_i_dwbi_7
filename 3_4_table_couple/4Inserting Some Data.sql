--insert functions=======================================================================================insert functions
print 'Inserting data in tables!!!!!!!!!!!!!!!!'
insert into gender(name) values('male'),('female')
insert into client (name, surname, age, gender_id) values ('Ihor', 'Stetskyi', 28, 0)
insert into country (name) values ('Ukraine')
insert into city (name, country_id) values ('Lviv', 0)
insert into adress (name, city_id) values ('Naukova1', 0)
insert into bar (name, foundation_date, capacity, bar_square, adress_id) values ('BestBar', GETDATE(), 50, 120, 0)
insert into provider (name) values ('BestProviderEver')
insert into beer (name, volume) values ('Lvivske', 0.5)
insert into bar_clients (bar_id, client_id) values (0, 0)
insert into avaiable_beer_in_bar (beer_id, bar_id) values(0, 0)
insert into orders (client_id, bar_id, beer_id, amount) values (0, 0, 0, 1)
go
