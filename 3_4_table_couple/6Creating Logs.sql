
--creatinc new schema===============================================================================================new schema
--SELECT * FROM sys.schemas

if (schema_id('MyLog') is not null)
begin
	print'deleting MyLog schema'
	drop schema MyLog
end
go




CREATE SCHEMA [MyLog] AUTHORIZATION [dbo]
go
--===============================================================================================creating table client 2
if (OBJECT_ID('client2') is not null)
begin
print'deleting table client2'
drop table client2
end
go
--creating table client
if (OBJECT_ID('client2') is null)
begin
print 'creating table client2'
create table client2
(
	id int primary key identity(0,1),
	name varchar(50) not null,
	surname varchar(50),
	age int constraint MustBeAtLeast18YearsOld2 check (age >= 18),
	client_weight float,
	birth_date date,
	registration_date datetime,
	adress_id int,
	gender_id int not null,
	favorite_bar_id int,
	favorite_beer_id int,
	inserted_date datetime default getdate(),
	updated_date datetime
	constraint UniqueNameSurname2 Unique(name, surname)
)
end
go






--===============================================================================================creating Log table for client2
if (OBJECT_ID('MyLog.client2') is not null)
begin
print'deleting table MyLog.client2'
drop table MyLog.client2
end
go
--creating table client
if (OBJECT_ID('MyLog.client2') is null)
begin
print 'creating table MyLog.client2'
create table MyLog.client2
(
	id int primary key identity(0,1),
	old_id int,
	name varchar(50),
	surname varchar(50),
	age int,
	client_weight float,
	birth_date date,
	registration_date datetime,
	adress_id int,
	gender_id int,
	favorite_bar_id int,
	favorite_beer_id int,
	inserted_date datetime,
	updated_date datetime,
	operation_type varchar(20),
	operation_date datetime
)
end
go





--=============================================================================================Deleting Trigger_client2
if (OBJECT_ID('Trigger_client2') is not null)
begin
	print'deleting Trigger_client2'
	drop trigger Trigger_client2
end
go
--=============================================================================================Creating Trigger_client2
CREATE TRIGGER Trigger_client2
	ON client2
	AFTER INSERT, UPDATE, DELETE
AS
	begin
		IF (@@ROWCOUNT = 0)
			print 'there are 0 rows inserted, updated or deleted'
		else
			update client set updated_date = GETDATE()
		    DECLARE @Result varchar(20)
			DECLARE @ins int = (select count(*) from INSERTED)
            DECLARE @del int = (select count(*) from DELETED)
			set @Result = 
			case 
				when @ins > 0 and @del > 0 then 'Update'  
				when @ins = 0 and @del > 0 then 'Delete'  
				when @ins > 0 and @del = 0 then 'Insert'  
			end

			--Insertion
			if @Result = 'Insert'
				begin
					insert into MyLog.client2(old_id,
											name,
											surname,
											age,
											client_weight,
											birth_date,
											registration_date,
											adress_id,
											gender_id,
											favorite_bar_id,
											favorite_beer_id,
											inserted_date,
											updated_date,
											operation_type,
											operation_date)

											Select  id,
													name,
													surname,
													age,
													client_weight,
													birth_date,
													registration_date,
													adress_id,
													gender_id,
													favorite_bar_id,
													favorite_beer_id,
													inserted_date,
													updated_date,
													@Result,
													GETDATE()			
											FROM inserted
				end
				--Deletion
			if @Result = 'Delete'
			begin
				insert into MyLog.client2(old_id,
											name,
											surname,
											age,
											client_weight,
											birth_date,
											registration_date,
											adress_id,
											gender_id,
											favorite_bar_id,
											favorite_beer_id,
											inserted_date,
											updated_date,
											operation_type,
											operation_date)

											Select  id,
													name,
													surname,
													age,
													client_weight,
													birth_date,
													registration_date,
													adress_id,
													gender_id,
													favorite_bar_id,
													favorite_beer_id,
													inserted_date,
													updated_date,
													@Result,
													GETDATE()			
											FROM deleted
			end
	end
GO

