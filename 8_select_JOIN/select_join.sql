use labor_sql
go

--1
select maker, [type], speed, hd from pc
inner join product on pc.model = product.model
where pc.hd<=8
go

--2
select distinct maker from pc
inner join product on pc.model = product.model
where speed >= 600
go

--3
select distinct maker from laptop
inner join product on laptop.model = product.model
where speed <= 500
go

--4
select distinct l1.model, l2.model, l1.hd, l1.ram, l2.hd, l2.ram from laptop as l1, laptop as l2
where l1.hd = l2.hd and l1.ram = l2.ram and l1.model > l2.model
go

--5
select c1.country, c1.class as bb_type, c2.class as bc_type from classes c1, classes c2
where c1.[type] = 'bb' and c2.[type] = 'bc' and c1.country = c2.country
go

--6
select pc.model, maker from pc
inner join product on product.model = pc.model
where price < 600
go

--7
select printer.model, maker from printer
inner join product on product.model = printer.model
where price > 300
go

--8
select maker, pc.model, price from pc
inner join product on product.model = pc.model
go

--9
select maker, product.model, price from product
left join pc on pc.model = product.model
where product.[type] = 'pc'
go

--10
select maker, [type], laptop.model, speed from laptop
inner join product on product.model = laptop.model
where  speed > 600
go

--11
select name, ships.class, launched, classes.displacement from ships
inner join classes on classes.class = ships.class
go

--12
select ship, battle, battles.[date] as battle_date from outcomes
inner join battles on outcomes.battle = battles.[name]
where result != 'sunk'

--13
select [name], country from ships
inner join classes on classes.class = ships.class
go

--14
select  trip_no,
		trip.id_comp,
		plane,
		town_from,
		town_to,
		time_out,
		time_in,
		company.[name] as company_name
from trip
inner join company on company.id_comp = trip.id_comp
where plane = 'Boeing'
go

--15
select [name], [date] from passenger
inner join pass_in_trip on pass_in_trip.id_psg = passenger.id_psg
go

--16
select pc.model, speed, hd from pc
inner join product on product.model = pc.model
where product.maker = 'A' and (pc.hd = 10 or pc.hd = 20)
go

--17
select maker, pc, laptop, printer from product
pivot
(
count([model]) for [type] in (pc, laptop, printer)
) as pivottable
go

--18
select 'Average price' as avg_, [11], [12], [14], [15] from
(select price, screen from laptop) as basetable
pivot
(
avg(price) for screen in ([11],[12],[14],[15])
) as pivottable
go

--19
select p.maker, l.* from laptop l
cross apply product p
where p.model = l.model
go

--20
select l1.*, a.max_price from laptop l1
inner join product p on p.model = l1.model
cross apply
(select maker, MAX(price) as max_price from laptop l2
inner join product on product.model = l2.model
group by maker) a
where p.maker = a.maker
order by code
go

--21
select * from laptop l1
cross apply
(select top 1 * from laptop l2 where l1.model < l2.model or (l1.model = l2.model and l1.code<l2.code) order by model, code) a
order by l1.model, l1.code
go

--22
select * from laptop l1
outer apply
(select top 1 * from laptop l2 where l1.model < l2.model or (l1.model = l2.model and l1.code<l2.code) order by model, code) a
order by l1.model, l1.code
go

--23
select x.* from (select distinct [type] from product) p1
cross apply
(select top 3 * from product p2 where p1.[type] = p2.[type] order by p2.model) x
go

--24
select [code], [name], [value] from laptop
cross apply
(
values ('speed', speed),
		('ram', ram),
		('hd', hd),
		('screen', screen)
) a([name],[value])
order by code, [name], [value]
go
