use [<I_Stetskyi>Library]
go
--alter old tables
go

--deleting old foreign key from BooksAuthors
if (OBJECT_ID('FK_BooksAuthors_books') is not null)
begin
print'deleting FK_BooksAuthors_books'
alter table BooksAuthors drop constraint FK_BooksAuthors_books
end
go

--deleting old primary key from Books
DECLARE @sql NVARCHAR(MAX);
SELECT @sql = 'ALTER TABLE Books DROP CONSTRAINT ' + name
	FROM sys.key_constraints
	WHERE [type] = 'PK'
	AND [parent_object_id] = OBJECT_ID('Books');
EXEC sp_executeSQL @sql;
go

--Alter Books
ALTER TABLE Books
ADD title varchar(50) not null default 'Title',
	edition int not null default 1 check (edition>=1),
	published date,
	issue int not null default 1
go

--adding primary key to books
ALTER TABLE Books
ADD CONSTRAINT PK_Books_new
PRIMARY KEY(ISBN, issue)
go

--adding foreign keys to BooksAuthors
begin
alter table BooksAuthors
ADD constraint FK_BooksAuthors_books FOREIGN KEY (ISBN, Seq_No) REFERENCES Books(ISBN, issue) on update cascade on delete no action
print 'created FK_BooksAuthors_books for table BooksAuthors'
end
go

--alter Authors
ALTER TABLE Authors
ADD birthday date,
	book_amount int not null default 0 CHECK (book_amount>=0),
	issue_amount int not null default 0 CHECK (issue_amount>=0),
	total_edition int not null default 0 CHECK (total_edition>=0)
go

--alter Publishers
ALTER TABLE Publishers
ADD created date not null default '01-01-1900',
	country varchar(50) not null default 'USA',
	City varchar(50) not null default 'NY',
	book_amount int not null default 0 check (book_amount>=0),
	issue_amount int not null default 0 check (issue_amount>=0),
	total_edition int not null default 0 check (total_edition>=0)
go

--alter Authors_log
ALTER TABLE Authors_log
ADD book_amount_old int,
	issue_amount_old int,
	total_edition_old int,
	book_amount_new int,
	issue_amount_new int,
	total_edition_new int
go





