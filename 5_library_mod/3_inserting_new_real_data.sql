use [<I_Stetskyi>Library]
go


--Inserting Real Data


insert into Authors (Authors_Id, [Name], birthday) values
 (NEXT VALUE FOR MySeq, 'Isaac Asimov','1920-01-02'),
 (NEXT VALUE FOR MySeq, 'Mark Twain', '1835-11-30'),
 (NEXT VALUE FOR MySeq, 'Robert Burns', '1759-01-25'),
 (NEXT VALUE FOR MySeq, 'Howard Phillips Lovecraft', '1890-08-20'),
 (NEXT VALUE FOR MySeq, 'Stanislaw Lem', '1921-09-12'),
 (NEXT VALUE FOR MySeq, 'Mikhail Bulgakov', '1891-05-15'),
 (NEXT VALUE FOR MySeq, 'Herbert George Wells', '1866-09-21'),
 (NEXT VALUE FOR MySeq, 'Stephen Edwin King', '1947-09-21'),
 (NEXT VALUE FOR MySeq, 'Yann Martel', '1963-06-25'),
 (NEXT VALUE FOR MySeq, 'Arthur Ignatius Conan Doyle', '1859-05-22')

insert into Publishers (Publisher_Id, [Name],created, country,city) values
 (NEXT VALUE FOR MySeq, 'Columbia Publications', '1937-08-21','USA','Springfield'),
 (NEXT VALUE FOR MySeq, 'Harper & Brothers','1817-04-06', 'USA', 'NY'),
 (NEXT VALUE FOR MySeq, 'John Wilson of Kilmarnock','1786-04-17', 'Scotland', 'Kilmarnock'),
 (NEXT VALUE FOR MySeq, 'Weird Tales', '1923-03-16','USA', 'NY'),
 (NEXT VALUE FOR MySeq, 'Walker','1959-01-30','USA', 'NY'),
 (NEXT VALUE FOR MySeq, 'YMCA Press','1921-05-05','Czech Republic','Prague'),
 (NEXT VALUE FOR MySeq, 'William Heinemann', '1890-07-25', 'England', 'London'),
 (NEXT VALUE FOR MySeq, 'Viking', '1925-04-21', 'USA', 'NY'),
 (NEXT VALUE FOR MySeq, 'Knopf Canada','1991-03-05','Canada','Toronto'),
 (NEXT VALUE FOR MySeq, 'Hodder & Stoughton','1868-06-13','England','London')

insert into Books (ISBN,Publisher_id,[URL], Title, edition, published, issue) values
 ('1000', 30,'http://www.multivax.com/last_question.html','The Last Question',10000, '1956-11-01', 1),
 ('2000', 31, 'https://www.gutenberg.org/files/3186/3186-h/3186-h.htm', 'The Mysterious Stranger', 15000, '1916-06-15', 1),
 ('3000', 32, 'https://archive.org/details/poemschieflyinsc03burn','Poems, chiefly in the Scottish Dialect', 1500, '1786-07-31',1),
 ('4000', 33, 'http://www.hplovecraft.com/writings/texts/fiction/cc.aspx', 'The Call of Cthulhu', 9000, '1926-02-09',1),
 ('0156027607', 34, 'http://loveread.ec/read_book.php?id=21726&p=48','Solaris',20000 ,'1961-06-23',1),
 ('0-14-118014-5', 35, 'https://www.masterandmargarita.eu/estore/pdf/eben001_mastermargarita_glenny.pdf','The Master and Margarita',25000,'1966-06-13',1),
 ('7000', 36, 'https://www.pagebypagebooks.com/H_G_Herbert_George_Wells/The_Time_Machine/I_p1.html','The Time Machine', 12000, '1895-08-06',1),
 ('0-670-81302-8', 37, 'https://www.readanybook.com/ebook/it-2-565296', 'It', 50000, '1986-09-15',1),
 ('0-676-97376-0', 38, 'https://www.scollingsworthenglish.com/uploads/3/8/4/2/38422447/_life_of_pi_full_text_pdf.pdf','Life of Pi',25000,'2001-09-11',1),
 ('10000', 39, 'http://www.planetpublish.com/wp-content/uploads/2011/11/The_Lost_World_NT.pdf','The Lost World',30000,'1912-01-12',1),
 ('1573920398', 30, 'https://www.goodreads.com/book/show/102564.The_Mysterious_Stranger','The Mysterious Stranger',200000,'1995-09-01',2),
 ('1001', 34,'https://www.goodreads.com/book/show/4808763-the-last-question','The Last Question',125000, '2007-11-01', 2),
 ('978-1-880418-62-8', 37, 'https://en.wikipedia.org/wiki/The_Dark_Tower_VII:_The_Dark_Tower', 'The Dark Tower', 250000, '2004-09-21',1),
 ('1111', 35,'https://www.ttu.ee/public/m/mart-murdvee/Techno-Psy/Isaac_Asimov_-_I_Robot.pdf','I, Robot',5000, '1950-01-02', 1)

insert into BooksAuthors (BooksAuthors_id ,[ISBN], Author_id, Seq_No) values
 (11,'1000', 20,1),
 (12,'2000', 21,1),
 (13,'3000', 22,1),
 (14,'4000', 23,1),
 (15,'0156027607', 24,1),
 (16,'0-14-118014-5', 25,1),
 (17,'7000', 26,1),
 (18,'0-670-81302-8', 27,1),
 (19,'0-676-97376-0', 28,1),
 (20,'10000', 29,1),
 (21,'1573920398', 21,2),
 (22,'1001', 20,2),
 (23,'978-1-880418-62-8', 27,1),
 (24,'1111', 20,1)
go

