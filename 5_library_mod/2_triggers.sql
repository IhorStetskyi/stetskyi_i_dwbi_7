use [<I_Stetskyi>Library]
go

--altering trigger trigger_Authors_for_U_D_I
create or alter trigger trigger_Authors_for_U_D_I
on Authors
for update, delete, insert 
as
	begin
	IF (@@ROWCOUNT = 0)
			print 'there are 0 rows inserted, updated or deleted'
		else
			update Authors set updated = GETDATE()
			from Authors inner join inserted on Authors.Authors_Id = inserted.Authors_Id
		    DECLARE @Result varchar(20)
			DECLARE @ins int = (select count(*) from INSERTED)
            DECLARE @del int = (select count(*) from DELETED)
			set @Result = 
			case 
				when @ins > 0 and @del > 0 then 'Update'  
				when @ins = 0 and @del > 0 then 'Delete'  
				when @ins > 0 and @del = 0 then 'Insert'  
			end
			print @Result
			--Insertion
if @Result = 'Insert'
				begin
					insert into Authors_log(Author_id_new,
											Name_new,
											URL_new,
											Author_id_old,
											Name_old,
											URL_old,
											operation_type,
											operation_datetime,
										    book_amount_new,
										    issue_amount_new,
										    total_edition_new)
					Select inserted.Authors_Id,
									inserted.[Name],
									inserted.[URL],
									Authors.Authors_Id,
									Authors.[Name],
									Authors.[URL],
									@Result,
									GETDATE(),
									inserted.book_amount,
									inserted.issue_amount,
									inserted.total_edition
						    FROM inserted inner join Authors on Authors.Authors_Id = inserted.Authors_Id
				end
--Deletion
if @Result = 'Delete'
				begin
					insert into Authors_log(Author_id_old,
											Name_old,
											URL_old,
											operation_type,
											operation_datetime,
											book_amount_old,
										    issue_amount_old,
										    total_edition_old)
					Select 			deleted.Authors_Id,
									deleted.[Name],
									deleted.[URL],
									@Result,
									GETDATE(),
									deleted.book_amount,
									deleted.issue_amount,
									deleted.total_edition
						    FROM [Authors] 
							INNER JOIN deleted ON Authors.Authors_Id = deleted.Authors_Id
				end
--Updation
if @Result = 'Update'
				begin
					insert into Authors_log(Author_id_new,
											Name_new,
											URL_new,
											Author_id_old,
											Name_old,
											URL_old,
											operation_type,
											operation_datetime,
											book_amount_old,
										    issue_amount_old,
										    total_edition_old,
										    book_amount_new,
										    issue_amount_new,
										    total_edition_new)
					Select inserted.Authors_Id,
									inserted.[Name],
									inserted.[URL],
									deleted.Authors_Id,
									deleted.[Name],
									deleted.[URL],
									@Result,
									GETDATE(),
									deleted.book_amount,
									deleted.issue_amount,
									deleted.total_edition,
									inserted.book_amount,
									inserted.issue_amount,
									inserted.total_edition
						    FROM [Authors]
							INNER JOIN  inserted ON Authors.Authors_Id = inserted.Authors_Id
							INNER JOIN deleted ON Authors.Authors_Id = deleted.Authors_Id
				end
				
	end
go


--creating trigger Books_Modification
if OBJECT_ID('Books_Modification') is not null
begin
	print 'deleting trigger Books_Modification'
	drop trigger Books_Modification
end
go

create trigger Books_Modification
on Books
after update, insert, delete
as
begin
		update Publishers
		set book_amount = temptable.book_amount,
			issue_amount = temptable.issue_amount,
			total_edition = temptable.total_edition
		from
		(select Books.Publisher_id, count(*)as book_amount, count(*)as issue_amount, SUM(edition) as total_edition 
		from Books where Books.Publisher_id in (select Publisher_id from Publishers)
		group by Books.Publisher_id) temptable
		where Publishers.Publisher_Id = temptable.Publisher_id

		update Publishers set book_amount = 0,
							  issue_amount = 0,
							  total_edition = 0
		where Publisher_id not in (select Publisher_Id from Books)
end
go

--creating trigger BooksAuthors_Modification
if OBJECT_ID('BooksAuthors_Modification') is not null
begin
	print 'deleting trigger BooksAuthors_Modification'
	drop trigger BooksAuthors_Modification
end
go

create trigger BooksAuthors_Modification
on BooksAuthors
after update, insert, delete
as
begin
		update Authors
		set book_amount = temptable.book_amount,
			issue_amount = temptable.issue_amount,
			total_edition = temptable.total_edition
		from
		(select Authors.Authors_Id, count(*)as book_amount, count(*)as issue_amount, SUM(edition) as total_edition from Books
		inner join BooksAuthors on BooksAuthors.ISBN = Books.ISBN
		inner join Authors on Authors_Id = BooksAuthors.Author_id
		group by Authors.Authors_Id) temptable
		where Authors.Authors_Id = temptable.Authors_id

		update Authors set book_amount = 0,
						   issue_amount = 0,
						   total_edition = 0
		where Authors_Id not in (select Authors_Id from BooksAuthors)
end
go