use labor_sql
go

--1
select maker, [type] from product
where [type] = 'laptop'
order by maker ASC
go

--2
select model, ram, screen, price from laptop
where price > 1000
order by ram asc, price desc
go

--3
select * from printer
where color = 'y'
order by price desc
go

--4
select model, speed, hd, cd, price
from pc
where cd in ('12x', '24x') and price < 600
order by speed desc
go

--5
select [name], class
from ships
where [name] = class
order by [name] asc
go

--6
select * from pc
where speed >= 500 and price < 800
order by price desc
go

--7
select * from printer
where [type] != 'Matrix' and price < 300
order by [type] desc
go

--8
select model, speed from pc
where price between 400 and 600
order by hd asc

--9
select model, speed, hd, price from laptop
where screen >= 12
order by price desc
go

--10
select model, [type], price from printer
where price < 300
order by [type] desc
go

--11
select model, ram, price from laptop
where ram = 64
order by screen asc
go

--12
select model, ram, price from pc
where ram > 64
order by hd asc
go

--13
select model, speed, price from pc
where speed between 500 and 750
order by hd desc
go

--14
select * from outcome_o
where [out] > 2000
order by [date] desc
go

--15
select * from income_o
where inc between 5000 and 10000
order by inc asc
go

--16
select * from income
where point = 1
order by inc asc
go

--17
select * from outcome
where point = 2
order by [out] asc
go

--18
select * from classes
where country = 'Japan'
order by [type] desc
go

--19
select [name], launched from ships
where launched between 1920 and 1942
order by launched desc
go

--20
select ship, battle, result from outcomes
where battle = 'Guadalcanal' and result != 'sunk'
order by ship desc
go

--21
select ship, battle, result from outcomes
where result = 'sunk'
order by ship desc
go

--22
select class, displacement from classes
where displacement >= 40000
order by [type] asc
go

--23
select trip_no, town_from, town_to from trip
where town_from = 'London' or town_to ='London'
order by  time_out asc
go

--24
select trip_no, plane, town_from, town_to from trip
where plane = 'TU-134'
order by time_out desc
go

--25
select trip_no, plane, town_from, town_to from trip
where plane != 'IL-86'
order by plane asc
go

--26
select trip_no, town_from, town_to from trip
where town_from != 'Rostov' and town_to != 'Rostov'
order by plane asc
go

--27
select * from pc
where (select len(model) - len(replace(model,'1',''))) >= 2
go

--28
select * from outcome
where month([date]) = 3
go

--29
select top 1 * from outcome_o
where day([date]) = 14

--30
select [name] from ships
where [name] like 'W%n'
go

--31
select [name] from ships
where (select len([name]) - len(replace([name],'e',''))) = 2
go

--32
select [name], launched from ships
where name not like '%a'
go

--33
select [name] from battles
where [name] like '% %' and [name] not like '%c'
go

--34
select * from trip
where time_out between '12:00:00' and '17:00:00'
go

--35
select * from trip
where time_in between '17:00:00' and '23:00:00'
go 

--36
select * from trip
where (datepart(hour, time_in) between 21 and 24) or (datepart(hour, time_in) between 0 and 10)
go

--37
select distinct [date] from pass_in_trip
where place like '1%'
go

--38
select distinct [date] from pass_in_trip
where place like '%c'
go

--39
select SUBSTRING([name], charindex(' ', [name])+1, 100) as [Surname] from passenger
where [name] like '% C%'
go

--40
select SUBSTRING([name], charindex(' ', [name])+1, 100) as [Surname] from passenger
where [name] not like '% J%'
go

--41
select  concat('������� ���� = ', avg(price)) as price from laptop
go

--42
select concat('���: ',code),
		concat('������: ', model),
		concat('�������� ��: ',speed, ' ��'),
		concat('���������� ���''���: ',ram, ' ��'),
		concat('�������� ����: ',hd, ' ��'),
		concat('�������� cd-�������: ',cd),
		concat('����: ',price,'$')
from pc
go

--43
select convert(varchar, [date], 102) from income
go

--44
declare @result varchar(50)
select ship, battle, result = 
	case result
			when 'OK' then '� �������'
			when 'damaged' then '�����������'
			when 'sunk' then '����������'
	end
from outcomes

--45
select concat('���: ',left(place,1)) as [row],
		concat('����: ',right(trim(place), 1)) as place
from pass_in_trip
go

--46
select trip_no, id_comp, plane, time_out, time_in, concat('From ',trim(town_from),' to ',town_to) as town_from_to from trip
go

--47
select * from trip 

select concat(left(trip_no, 1),
				right(trip_no,1),
				left(id_comp, 1),
				right(id_comp,1),
				left(plane, 1),
				right(trim(plane),1),
				left(town_from, 1),
				right(trim(town_from),1),
				left(town_to, 1),
				right(trim(town_to),1),
				left(time_out,1),
				right(time_out,1),
				left(time_in, 1),
				right(time_in,1)
				) 
from trip
go

--48
select maker, count(distinct model) as model_count from product
where [type] = 'pc'
group by maker
having count(model) >= 2

--49
select city, count(*) as [count] from
(
	select town_from as city from trip
	union all
	select town_to as city from trip
) temp
group by city

--50
select [type], count(model) from printer
group by [type]
go

--51
select * from
(select model, count(distinct cd) as amount_cd from pc
group by model
) aa
full outer join
(select * from
(
select cd, count(*) as amount_model from
(
select distinct pc.cd, temp.model from pc
inner join (select model, cd from pc) temp on pc.cd = temp.cd
) a
group by cd
) bb) cc on aa.model = cc.cd

--52
select trip_no, cast((time_in - time_out) as time(0)) as 'time in flight' from trip
go

--53
select coalesce(cast(s.point as varchar(20)),'All points') as point,
		coalesce(cast(s.[date]as varchar(20)), 'Total') as [date],
		sum(out) as [sum],
		(select max(out) from outcome where s.point = outcome.point) as [max],
		(select min(out) from outcome where s.point = outcome.point) as [min]
from outcome s
group by s.point, s.[date] with rollup
go

--54
select trip_no,
		left(place,1) as [row],
		count(*) as place_count
from pass_in_trip
group by trip_no, left(place,1)
order by trip_no
go

--55
select count([name]) as passangers_count from passenger
where [name] like '% [S,B,A]%'
go

