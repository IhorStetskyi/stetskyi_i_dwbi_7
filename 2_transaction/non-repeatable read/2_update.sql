use products
go

begin transaction
	update product
	set [name] = 'TottalyNotPotatos'
	where id = 1
commit transaction