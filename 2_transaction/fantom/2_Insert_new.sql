use products
go


--DBCC CHECKIDENT ('product', RESEED, 3)
--GO

SET IDENTITY_INSERT product ON;  
GO

begin transaction
insert into product (id, [name], unit_price, description_id) values (3, 'NewProduct', 11.11, 1)
commit transaction
go

SET IDENTITY_INSERT product OFF;  
GO




