use people_ua_db
go
--Ceating Table People
if OBJECT_ID('People') is not null
	drop table People
go

if OBJECT_ID('People') is null
begin
	create table People
	(
		id int primary key identity(0, 1) not null,
		surname_list_identity_id int,
		name_list_identity_id int
	)
end
go


--creating cursor
declare cur cursor for 
select id, sex, amount from surname_list_identity
go

--inserting data into table People
open cur
declare @surname int
declare @sex nchar
declare @amount int
declare @amount_to_go int
declare @names_amount int
declare @name_id int
fetch next from cur into @surname, @sex, @amount
set @names_amount = cast((select count(*) from name_list_identity) as int)
while @@FETCH_STATUS = 0
begin
	set @amount_to_go = @amount
	while @amount_to_go > 0
	begin
			if(@sex is null)
			begin
				set @name_id = cast(RAND()*(@names_amount)+1 as int);
				insert into People (surname_list_identity_id, name_list_identity_id) values (@surname, @name_id)
				set @amount_to_go = @amount_to_go - 1
			end
			else if (@sex = 'm')
				begin
					while 1 = 1
					begin
						set @name_id = cast(RAND()*(@names_amount)+1 as int)
						if(@sex = cast((select sex from name_list_identity where id = @name_id) as nchar))
						break
					end
						insert into People (surname_list_identity_id, name_list_identity_id) values (@surname, @name_id)
						set @amount_to_go = @amount_to_go - 1
				end
			else if (@sex = 'w')
				begin
					while 1 = 1
					begin
						set @name_id = cast(RAND()*(@names_amount)+1 as int)
						if(@sex = cast((select sex from name_list_identity where id = @name_id) as nchar))
						break
					end
						insert into People (surname_list_identity_id, name_list_identity_id) values (@surname, @name_id)
						set @amount_to_go = @amount_to_go - 1
				end
	end	
	fetch next from cur into @surname, @sex, @amount
	--continue
end
close cur
go

--deleting cursor
deallocate cur
go

--selecting data from People
Select People.id, surname_list_identity.surname, name_list_identity.[name] from People
inner join surname_list_identity on People.surname_list_identity_id = surname_list_identity.id
inner join name_list_identity on People.name_list_identity_id = name_list_identity.id
--order by People.id
go