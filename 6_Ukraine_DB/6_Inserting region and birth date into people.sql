use people_ua_db
go

--creating 2 columns
IF not EXISTS(SELECT 1 FROM sys.columns WHERE Name = 'region_id' AND Object_ID = Object_ID('People'))
 and not EXISTS(SELECT 1 FROM sys.columns WHERE Name = 'birth_date' AND Object_ID = Object_ID('People'))
	begin
		ALTER TABLE People
		ADD region_id int,
			birth_date datetime
	end
	go


	alter database people_ua_db
	set recovery simple
	go

--filling region_id and birth_date
declare @rows_amount int
declare @current_row int
declare @days int
declare @regions_amount int
declare @max_insert_per_transaction int

set @rows_amount = cast((select count(*) from People) as int)
set @current_row = cast((select top (1) id from People where region_id is null or birth_date is null) as int)
set @days = cast(DATEDIFF(day, '1925-01-01', '2000-01-01') AS int)
set @regions_amount = cast((select count(*) from region) as int)
set @max_insert_per_transaction = @current_row + 45000000

while @current_row < @max_insert_per_transaction and @current_row < @rows_amount
	begin
		update People set 
				birth_date = DATEADD(day, cast(RAND()*(@days)+1 as int), '1925-01-01'),
				region_id = cast(rand()*(@regions_amount)+1 as int)
				where id = @current_row
		set @current_row = @current_row +1
	end
go

alter database people_ua_db
	set recovery full
	go

