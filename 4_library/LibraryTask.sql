USE master;
--deleting <I_Stetskyi> database=================================================================================database
if DB_ID('<I_Stetskyi>Library') is not null
begin
print 'deleting database <I_Stetskyi>Library'
ALTER DATABASE [<I_Stetskyi>Library] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
drop database [<I_Stetskyi>Library]
end
go
--creating <I_Stetskyi>_Library database
if DB_ID('<I_Stetskyi>Library') is null
begin
print 'creating database <I_Stetskyi>Library'
create database [<I_Stetskyi>Library]
end
go




--add filegroup DATA
alter database [<I_Stetskyi>Library]
add filegroup [DATA]
--add File DATA
ALTER DATABASE [<I_Stetskyi>Library] ADD FILE (NAME='DATA', FILENAME='C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DATA.ndf') TO FILEGROUP [DATA];
--set filegroup DATA as default
ALTER DATABASE [<I_Stetskyi>Library]
MODIFY FILEGROUP [DATA] DEFAULT
go









use [<I_Stetskyi>Library];

---- sequence creation
drop sequence if exists MySeq
go

create sequence MySeq 
as int
start with 0
increment by 1
go



--creating tables============================================================================================creating tables
if (OBJECT_ID('Authors') is not null)
begin
print'deleting table Authors'
drop table Authors
end
go
--creating table Authors
if (OBJECT_ID('Authors') is null)
begin
print 'creating table Authors'
create table Authors
(
	Authors_Id int primary key not null,
	[Name] varchar(50) Unique not null,
	[URL] varchar(50) not null default 'www.author_name.com',
	[inserted] datetime not null default getdate(),
	[inserted_by] varchar(50) not null default system_user,
	[updated] datetime,
	[updated_by] varchar(50)
)
end
go


if (OBJECT_ID('Books') is not null)
begin
print'deleting table Books'
drop table Books
end
go
--creating table Books
if (OBJECT_ID('Books') is null)
begin
print 'creating table Books'
create table Books
(
	ISBN varchar(50) primary key not null,
	Publisher_id int not null,
	[URL] varchar(100) Unique not null,
	Price money not null CHECK ([Price]>=0) default 0,
	[inserted] datetime not null default getdate(),
	[inserted_by] varchar(50) not null default system_user,
	[updated] datetime,
	[updated_by] varchar(50)
)
end
go


if (OBJECT_ID('BooksAuthors') is not null)
begin
print'deleting table BooksAuthors'
drop table BooksAuthors
end
go
--creating table BooksAuthors
if (OBJECT_ID('BooksAuthors') is null)
begin
print 'creating table BooksAuthors'
create table BooksAuthors
(
	BooksAuthors_id int primary key not null default 1 CHECK (BooksAuthors_id>=1),
	[ISBN] varchar(50) unique not null,
	Author_id int not null,
	Seq_No int not null default 1 CHECK (Seq_No>=1),
	[inserted] datetime not null default getdate(),
	[inserted_by] varchar(50) not null default system_user,
	[updated] datetime,
	[updated_by] varchar(50)
)
end
go


if (OBJECT_ID('Authors_log') is not null)
begin
print'deleting table Authors_log'
drop table Authors_log
end
go
--creating table Authors_log
if (OBJECT_ID('Authors_log') is null)
begin
print 'creating table Authors_log'
create table Authors_log
(
	operation_id int primary key identity(0,1) not null,
	Author_id_new int,
	Name_new varchar(50),
	URL_new varchar(50),
	Author_id_old int,
	Name_old varchar(50),
	URL_old varchar(50),
	operation_type varchar(50) not null,
	operation_datetime varchar(50) not null default getdate()
)
end
go


if (OBJECT_ID('Publishers') is not null)
begin
print'deleting table Publishers'
drop table Publishers
end
go
--creating table Publishers
if (OBJECT_ID('Publishers') is null)
begin
print 'creating table Publishers'
create table Publishers
(
	Publisher_Id int primary key not null,
	[Name] varchar(50) unique not null,
	[URL] varchar(50) not null default 'www.publisher_name.com',
	[inserted] datetime not null default getdate(),
	[inserted_by] varchar(50) not null default system_user,
	[updated] datetime,
	[updated_by] varchar(50)
)
end
go



--creating foreign keys
--=================================================================================================================================FK
if (OBJECT_ID('FK_Books_Publisher') is not null)
begin
print'deleting FK_Books_Publisher'
alter table Books drop constraint FK_Books_Publisher
end
go


begin
alter table Books
ADD constraint FK_Books_Publisher FOREIGN KEY (Publisher_id) REFERENCES Publishers(Publisher_Id)
print 'created FK for table Books'
end
go

if (OBJECT_ID('FK_BooksAuthors_books') is not null)
begin
print'deleting FK_BooksAuthors_books'
alter table BooksAuthors drop constraint FK_BooksAuthors_books
end
go

if (OBJECT_ID('FK_BooksAuthors_authors') is not null)
begin
print'deleting FK_BooksAuthors_authors'
alter table BooksAuthors drop constraint FK_BooksAuthors_authors
end
go

begin
alter table BooksAuthors
ADD constraint FK_BooksAuthors_books FOREIGN KEY (ISBN) REFERENCES Books(ISBN) on update cascade on delete no action,
	constraint FK_BooksAuthors_authors FOREIGN KEY (Author_id) REFERENCES Authors(Authors_Id) on update no action on delete no action
print 'created FK for table BooksAuthors'
end
go





--===================================================================================================================triggers

--create trigger trigger_authors
--on Authors
--for update
--as
--	begin
--		if(@@ROWCOUNT = 0)
--		print 'there is nothing to update'
--		else
--		update Authors set updated = GETDATE(), updated_by = SYSTEM_USER
--		from Authors inner join inserted on Authors.Authors_Id = inserted.Authors_Id
--	end
--go



if (OBJECT_ID('trigger_Books') is not null)
begin
	print'deleting trigger_Books'
	drop trigger trigger_Books
end
go

create trigger trigger_Books
on Books
for update
as
	begin
		if(@@ROWCOUNT = 0)
		print 'there is nothing to update'
		else
		update Books set updated = GETDATE(), updated_by = SYSTEM_USER
		from Books inner join inserted on Books.ISBN = inserted.ISBN
	end
go


if (OBJECT_ID('trigger_BooksAuthors') is not null)
begin
	print'deleting trigger_BooksAuthors'
	drop trigger trigger_BooksAuthors
end
go

create trigger trigger_BooksAuthors
on BooksAuthors
for update
as
	begin
		if(@@ROWCOUNT = 0)
		print 'there is nothing to update'
		else
		update BooksAuthors set updated = GETDATE(), updated_by = SYSTEM_USER
		from BooksAuthors inner join inserted on BooksAuthors.BooksAuthors_id = inserted.BooksAuthors_id
	end
go

if (OBJECT_ID('trigger_Publishers') is not null)
begin
	print'deleting trigger_Publishers'
	drop trigger trigger_Publishers
end
go

create trigger trigger_Publishers
on Publishers
for update
as
	begin
		if(@@ROWCOUNT = 0)
		print 'there is nothing to update'
		else
		update Publishers set updated = GETDATE(), updated_by = SYSTEM_USER
		from Publishers inner join inserted on Publishers.Publisher_Id = inserted.Publisher_Id
	end
go

if (OBJECT_ID('trigger_authors_log') is not null)
begin
	print'deleting trigger_authors_log'
	drop trigger trigger_authors_log
end
go

create trigger trigger_authors_log
on Authors_log
INSTEAD OF DELETE 
as
	begin
	print 'You can not delete from Authors_log'
	end
go

if (OBJECT_ID('trigger_Authors_for_U_D_I') is not null)
begin
	print'deleting trigger_Authors_for_U_D_I'
	drop trigger trigger_Authors_for_U_D_I
end
go

create or alter trigger trigger_Authors_for_U_D_I
on Authors
for update, delete, insert 
as
	begin
	IF (@@ROWCOUNT = 0)
			print 'there are 0 rows inserted, updated or deleted'
		else
			update Authors set updated = GETDATE()
			from Authors inner join inserted on Authors.Authors_Id = inserted.Authors_Id
		    DECLARE @Result varchar(20)
			DECLARE @ins int = (select count(*) from INSERTED)
            DECLARE @del int = (select count(*) from DELETED)
			set @Result = 
			case 
				when @ins > 0 and @del > 0 then 'Update'  
				when @ins = 0 and @del > 0 then 'Delete'  
				when @ins > 0 and @del = 0 then 'Insert'  
			end
			print @Result
			--Insertion
if @Result = 'Insert'
				begin
					insert into Authors_log(Author_id_new,
											Name_new,
											URL_new,
											Author_id_old,
											Name_old,
											URL_old,
											operation_type,
											operation_datetime)
					Select inserted.Authors_Id,
									inserted.[Name],
									inserted.[URL],
									Authors.Authors_Id,
									Authors.[Name],
									Authors.[URL],
									@Result,
									GETDATE()
						    FROM inserted inner join Authors on Authors.Authors_Id = inserted.Authors_Id
				end
--Deletion
if @Result = 'Delete'
				begin
					insert into Authors_log(Author_id_new,
											Name_new,
											URL_new,
											Author_id_old,
											Name_old,
											URL_old,
											operation_type,
											operation_datetime)
					Select null,
									null,
									null,
									deleted.Authors_Id,
									deleted.[Name],
									deleted.[URL],
									@Result,
									GETDATE()
						    FROM deleted
				end
--Updation
if @Result = 'Update'
				begin
					insert into Authors_log(Author_id_new,
											Name_new,
											URL_new,
											Author_id_old,
											Name_old,
											URL_old,
											operation_type,
											operation_datetime)
					Select inserted.Authors_Id,
									inserted.[Name],
									inserted.[URL],
									deleted.Authors_Id,
									deleted.[Name],
									deleted.[URL],
									@Result,
									GETDATE()
						    FROM inserted inner join deleted on inserted.Authors_Id = deleted.Authors_Id
				end
				
	end
go





--==========================================================inserting data
insert into Authors (Authors_Id, [Name]) values
 (NEXT VALUE FOR MySeq, 'Name1'),
 (NEXT VALUE FOR MySeq, 'Name2'),
 (NEXT VALUE FOR MySeq, 'Name3'),
 (NEXT VALUE FOR MySeq, 'Name4'),
 (NEXT VALUE FOR MySeq, 'Name5'),
 (NEXT VALUE FOR MySeq, 'Name6'),
 (NEXT VALUE FOR MySeq, 'Name7'),
 (NEXT VALUE FOR MySeq, 'Name8'),
 (NEXT VALUE FOR MySeq, 'Name9'),
 (NEXT VALUE FOR MySeq, 'Name10')

 

insert into Publishers (Publisher_Id, [Name]) values
 (NEXT VALUE FOR MySeq, 'publishername1'),
 (NEXT VALUE FOR MySeq, 'publishername2'),
 (NEXT VALUE FOR MySeq, 'publishername3'),
 (NEXT VALUE FOR MySeq, 'publishername4'),
 (NEXT VALUE FOR MySeq, 'publishername5'),
 (NEXT VALUE FOR MySeq, 'publishername6'),
 (NEXT VALUE FOR MySeq, 'publishername7'),
 (NEXT VALUE FOR MySeq, 'publishername8'),
 (NEXT VALUE FOR MySeq, 'publishername9'),
 (NEXT VALUE FOR MySeq, 'publishername10')

insert into Books (ISBN,Publisher_id,[URL]) values
 ('0001', 10, 'myurl1'),
 ('0002', 11, 'myurl2'),
 ('0003', 12, 'myurl3'),
 ('0004', 13, 'myurl4'),
 ('0005', 14, 'myurl5'),
 ('0006', 15, 'myurl6'),
 ('0007', 16, 'myurl7'),
 ('0008', 17, 'myurl8'),
 ('0009', 18, 'myurl9'),
 ('0010', 19, 'myurl10')

insert into BooksAuthors (BooksAuthors_id ,[ISBN], Author_id) values
 (1,'0001', 0),
 (2,'0002', 1),
 (3,'0003', 2),
 (4,'0004', 3),
 (5,'0005', 4),
 (6,'0006', 5),
 (7,'0007', 6),
 (8,'0008', 7),
 (9,'0009', 8),
 (10,'0010', 9)
go



--==================================================================================================================views

if (OBJECT_ID('view_books_authors') is not null)
begin
	print'deleting view_books_authors'
	drop view view_books_authors
end
go

create view view_books_authors
as
select Books.[ISBN], Authors.[Name] as Author_name, Publishers.[Name] as Publisher_name
 from BooksAuthors
 inner join Authors on BooksAuthors.Author_id = Authors.Authors_Id
 inner join Books on BooksAuthors.ISBN = Books.ISBN
 inner join Publishers on Books.Publisher_id = Publishers.Publisher_Id
 go




 use master
 go

 if OBJECT_ID('Authors') is not null
 begin
	 print 'deleting authors'
	 drop synonym dbo.Authors
 end
 go

  if OBJECT_ID('Books') is not null
 begin
	 print 'deleting Books'
	 drop synonym dbo.Books
 end
 go

  if OBJECT_ID('Authors_log') is not null
 begin
	 print 'deleting Authors_log'
	 drop synonym dbo.Authors_log
 end
 go

  if OBJECT_ID('BooksAuthors') is not null
 begin
	 print 'deleting BooksAuthors'
	 drop synonym dbo.BooksAuthors
 end
 go

  if OBJECT_ID('Publishers') is not null
 begin
	 print 'deleting Publishers'
	 drop synonym dbo.Publishers
 end
 go



 --deleting synonyms=======================================================================================================================
 if (OBJECT_ID('dbo.Authors') is not null)
begin
	print'deleting dbo.Authors'
	drop synonym dbo.Authors
end
go
 if (OBJECT_ID('dbo.Books') is not null)
begin
	print'deleting dbo.Books'
	drop synonym dbo.Books
end

go
 if (OBJECT_ID('dbo.Authors_log') is not null)
begin
	print'deleting dbo.Authors_log'
	drop synonym dbo.Authors_log
end
go
 if (OBJECT_ID('dbo.BooksAuthors') is not null)
begin
	print'deleting dbo.BooksAuthors'
	drop synonym dbo.BooksAuthors
end
go
 if (OBJECT_ID('dbo.Publishers') is not null)
begin
	print'deleting dbo.Publishers'
	drop synonym dbo.Publishers
end
go

--creating synonyms==========================================================================================================================
 create synonym dbo.Authors for [<I_Stetskyi>Library].dbo.Authors
 create synonym dbo.Books for [<I_Stetskyi>Library].dbo.Books
 create synonym dbo.Authors_log for [<I_Stetskyi>Library].dbo.Authors_log
 create synonym dbo.BooksAuthors for [<I_Stetskyi>Library].dbo.BooksAuthors
 create synonym dbo.Publishers for [<I_Stetskyi>Library].dbo.Publishers
 go



--insert into Authors_log (operation_type, operation_datetime) values ('deletionsss', GETDATE())
--select * from Authors_log
--delete from Authors_log where operation_id = 0

--select * from Authors
--select * from Authors_log
--delete from Authors where Authors_Id = 1
--delete from Authors where Authors_Id > 1 and Authors_Id < 7

--update Authors set [Name] = 'newName' where Authors_Id = 0

-- select * from view_books_authors
 
 
