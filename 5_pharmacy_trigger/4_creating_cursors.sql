use triger_fk_cursor
go


--creating tables for each name in employees
declare cur cursor for 
select [name] from employee
go

open cur
declare @name varchar(50)
declare @coloumns_amount int
declare @col int
set @col = 2
fetch next from cur into @name
while @@FETCH_STATUS = 0
	begin
		set @coloumns_amount = cast(RAND()*9 as int);
		exec('create table '+@name+'(Coloumn1 int)')
		while @coloumns_amount > 0
			begin
				exec('alter table '+@name+' add Coloumn'+@col+' int')
				set @col = (@col+1)
				set @coloumns_amount = (@coloumns_amount - 1)
			end
		fetch next from cur into @name
		set @col = 2
	end
	close cur
	deallocate cur
go



--creating stored procedure for creating 2 tables
if OBJECT_ID('creatingtables') is not null
begin
print 'deleting stored procedure creatingtables'
drop procedure creatingtables
end
go

create procedure creatingtables
as
begin
if OBJECT_ID ('TableA') is not null
	begin
		print 'deleting TableA'
		drop table [TableA]
	end
if OBJECT_ID ('TableB') is not null
begin
	print 'deleting TableB'
	drop table [TableB]
end
print 'creating TableA'
	CREATE TABLE [TableA](
		id                 INT               IDENTITY(1,1),
		surname            VARCHAR(30)       NOT NULL,
		name               CHAR(30)          NOT NULL,
		midle_name         VARCHAR(30),
		identity_number    CHAR(10),
		passport           CHAR(10),
		experience         DECIMAL(10, 1),
		birthday           DATE,
		post               VARCHAR(15)       NOT NULL,
		pharmacy_id        INT,
		cur_time		   DateTime,
		PRIMARY KEY (id)
	)
print 'creating TableB'
	CREATE TABLE [TableB](
		id                 INT               IDENTITY(1,1),
		surname            VARCHAR(30)       NOT NULL,
		name               CHAR(30)          NOT NULL,
		midle_name         VARCHAR(30),
		identity_number    CHAR(10),
		passport           CHAR(10),
		experience         DECIMAL(10, 1),
		birthday           DATE,
		post               VARCHAR(15)       NOT NULL,
		pharmacy_id        INT,
		cur_time		   DateTime,
		PRIMARY KEY (id)
	)
end
go



--creating 2 tables with time seed
declare cur2 cursor for 
select surname,[name],midle_name,identity_number,passport,experience,birthday,post,pharmacy_id from employee
go

open cur2
exec creatingtables
declare @surname            VARCHAR(30),
		@name               CHAR(30),
		@midle_name         VARCHAR(30),
		@identity_number    CHAR(10),
		@passport           CHAR(10),
		@experience         DECIMAL(10, 1),
		@birthday           DATE,
		@post               VARCHAR(15),
		@pharmacy_id        INT
fetch next from cur2 into @surname,@name,@midle_name,@identity_number,@passport,@experience,@birthday,@post,@pharmacy_id
while @@FETCH_STATUS = 0
	begin
	declare @randBIN int
	set @randBIN = cast(RAND()*2 as int)
	if @randBIN > 0
		begin
			insert into TableA (surname,[name],midle_name,identity_number,passport,experience,birthday,post,pharmacy_id,cur_time) values
			(@surname,@name,@midle_name,@identity_number,@passport,@experience,@birthday,@post,@pharmacy_id,GETDATE())
		end
	else
		begin
			insert into TableB (surname,[name],midle_name,identity_number,passport,experience,birthday,post,pharmacy_id,cur_time) values
			(@surname,@name,@midle_name,@identity_number,@passport,@experience,@birthday,@post,@pharmacy_id,GETDATE())
		end
	fetch next from cur2 into @surname,@name,@midle_name,@identity_number,@passport,@experience,@birthday,@post,@pharmacy_id
	end
	close cur2
	deallocate cur2
go















