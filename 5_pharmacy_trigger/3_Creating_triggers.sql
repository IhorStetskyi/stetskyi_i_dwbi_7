use triger_fk_cursor
go

--creating triggers

--employee
if OBJECT_ID('employee_trigger1') is not null
begin
drop trigger [employee_trigger1]
end
go

create trigger employee_trigger1
on employee
for insert, update
as
	begin
		if (@@ROWCOUNT = 0)
			print 'there are 0 rows in update or insert'
		else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
		declare @a int
		declare @b int
			set @a = (select count(post) from employee where post = 'director')
			set @b = (select count(post) from inserted where post = 'director')
			if @delete = 0
				begin
						if (cast((select count(employee.id) from employee inner join inserted on employee.[name] = inserted.[name] and employee.surname = inserted.surname)as int)<=cast((select count(*) from inserted)as int))
							begin
								if not exists (select inserted.[post] from inserted except select post from post)
								begin
										if ((@a > 1) and (@b > 0))
											begin
												print 'there already is a director'
												rollback transaction
											end
										else
											begin
												if exists (select identity_number from inserted where identity_number like '%00')
													begin
														print 'identity_number cannot end with 00'
														rollback transaction
													end
											end
								end
								else
									begin
										print 'there is no such post'
										rollback transaction
									end
							end
						else
							begin
								print 'there is already such person'
								rollback transaction
							end
				end
			else
				begin
				--declare @aaa int
				--set @aaa = (select count(employee.id) from employee inner join inserted on employee.[name] = inserted.[name] and employee.surname = inserted.surname and employee.post = inserted.post)
						if not exists (SELECT [surname], [name], COUNT(*) FROM employee GROUP BY [surname], [name] HAVING COUNT(*) > 1)
							begin
							--select [name], [surname] from employee intersect select [name], [surname] from inserted
							--print @aaa
								if not exists (select inserted.[post] from inserted except select post from post)
								begin
										if ((@a > 1) and (@b > 0))
											begin
												print 'there already is a director'
												rollback transaction
											end
										else
											begin
												if exists (select identity_number from inserted where identity_number like '%00')
													begin
														print 'identity_number cannot end with 00'
														rollback transaction
													end
											end
								end
								else
									begin
										print 'there is no such post'
										rollback transaction
									end
							end
						else
							begin
								begin
									print 'there is already such person'
									rollback transaction
								end
							end
				end
		end
	end
go

if object_id ('medicine_zone_trigger1') is not null
begin
drop trigger [medicine_zone_trigger1]
end
go

create trigger medicine_zone_trigger1
on medicine_zone
for insert, update
as
begin
	if (@@ROWCOUNT = 0)
		print 'there are 0 rows in update or insert'
	else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(*) from medicine_zone inner join inserted on medicine_zone.medicine_id = inserted.medicine_id and medicine_zone.zone_id = inserted.zone_id) as int)<=cast((select count(*) from inserted)as int))
							begin
								if (not exists (select inserted.medicine_id from inserted except select id from medicine) and not exists (select inserted.zone_id from inserted except select id from [zone]))
								begin
									print 'done'
								end
								else
									begin
										print 'there is no such zone or medicine'
										rollback transaction
									end
							end
						else
							begin
								print 'there is already such medicine zone'
								rollback transaction
							end
				end
			else
				begin
						if not exists (SELECT [medicine_id], [zone_id], COUNT(*) FROM medicine_zone GROUP BY [medicine_id], [zone_id] HAVING COUNT(*) > 1)
							begin
								if (not exists (select inserted.medicine_id from inserted except select id from medicine) and not exists (select inserted.zone_id from inserted except select id from [zone]))
								begin
									print 'done'
								end
								else
									begin
										print 'there is no such zone or medicine'
										rollback transaction
									end
							end
						else
							begin
								print 'there is already such medicine zone'
								rollback transaction
							end
				end

		end
end
go

if object_id ('medicine_trigger1') is not null
begin
drop trigger [medicine_trigger1]
end
go

create trigger medicine_trigger1 --(with cascade update (not really nessesary))
on medicine
for insert, update
as
begin
	if (@@ROWCOUNT = 0)
		print 'there are 0 rows in update or insert'
	else
			begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(*) from medicine inner join inserted on medicine.[name] = inserted.[name] and medicine.ministry_code = inserted.ministry_code) as int)<=cast((select count(*) from inserted)as int))
							begin
								if (exists (select ministry_code from inserted where ministry_code not like '[a-z][a-z]-[0-9][0-9][0-9]-[0-9][0-9]')) or (exists (select ministry_code from inserted where ministry_code like '[m,p]_-[0-9][0-9][0-9]-[0-9][0-9]')) or (exists (select ministry_code from inserted where ministry_code like '_[m,p]-[0-9][0-9][0-9]-[0-9][0-9]'))
									begin
										print 'ministry code should be like LL-NNN-NN Letters should not be M or P'
										rollback transaction
									end
							end
						else
							begin
								print 'there is already such medicine'
								rollback transaction
							end
				end
			else
				begin
						if not exists (SELECT [name], [ministry_code], COUNT(*) FROM medicine GROUP BY [name], [ministry_code] HAVING COUNT(*) > 1)
							begin
							if (exists (select ministry_code from inserted where ministry_code not like '[a-z][a-z]-[0-9][0-9][0-9]-[0-9][0-9]')) or (exists (select ministry_code from inserted where ministry_code like '[m,p]_-[0-9][0-9][0-9]-[0-9][0-9]')) or (exists (select ministry_code from inserted where ministry_code like '_[m,p]-[0-9][0-9][0-9]-[0-9][0-9]'))
									begin
										print 'ministry code should be like LL-NNN-NN Letters should not be M or P'
										rollback transaction
									end
							else
								begin
									declare medicine_cur1 cursor for 
									select id from deleted
									open medicine_cur1

									declare medicine_cur2 cursor for 
									select id from inserted
									open medicine_cur2

									declare @iddel int
									declare @idins int
									fetch next from medicine_cur1 into @iddel
									fetch next from medicine_cur2 into @idins
									while @@FETCH_STATUS = 0
										begin
											update medicine_zone set medicine_id = @idins where medicine_id = @iddel
											update pharmacy_medicine set medicine_id = @idins where medicine_id = @iddel
											fetch next from medicine_cur1 into @iddel
											fetch next from medicine_cur2 into @idins
										end
									close medicine_cur1
									close medicine_cur2
									deallocate medicine_cur1
									deallocate medicine_cur2
								end
							end
						else
							begin
								print 'there is already such medicine'
								rollback transaction
							end
				end

		end

end
go

if object_id ('parmacy_trigger1') is not null
begin
drop trigger parmacy_trigger1
end
go

create trigger parmacy_trigger1
on pharmacy
for insert, update
as
begin
		if (@@ROWCOUNT = 0)
			print 'there are 0 rows in update or insert'
		else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(pharmacy.id) from pharmacy inner join inserted on pharmacy.[name] = inserted.[name] and pharmacy.building_number = inserted.building_number and pharmacy.street = inserted.street)as int)<=cast((select count(*) from inserted)as int))
							begin
								if not exists (select inserted.street from inserted except select street from street)
									begin
										print 'done'
									end
								else
									begin
										print 'there is no such street'
										rollback transaction
									end
							end
						else
								begin
									print 'there is already such pharmacy'
									rollback transaction
								end
				end
			else
				begin
						if not exists (SELECT [name], [building_number], street, COUNT(*) FROM pharmacy GROUP BY [name], [building_number], street HAVING COUNT(*) > 1)
							begin
								if (not exists (select inserted.[street] from inserted except select street from street)) or not exists (select street from inserted where street is not null) 
								begin
									print 'done'
								end
								else
									begin
										print 'there is no such street'
										rollback transaction
									end
							end
						else
								begin
									print 'there is already such pharmacy'
									rollback transaction
								end
				end
		end
end
go

if object_id ('pharmacy_medicine_trigger1') is not null
begin
drop trigger pharmacy_medicine_trigger1
end
go

create trigger pharmacy_medicine_trigger1
on pharmacy_medicine
for insert, update
as
begin
	if (@@ROWCOUNT = 0)
		print 'there are 0 rows in update or insert'
	else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(*) from pharmacy_medicine inner join inserted on pharmacy_medicine.medicine_id = inserted.medicine_id and pharmacy_medicine.pharmacy_id = inserted.pharmacy_id) as int)<=cast((select count(*) from inserted)as int))
							begin
								if (not exists (select inserted.medicine_id from inserted except select id from medicine) and not exists (select inserted.pharmacy_id from inserted except select id from pharmacy))
								begin
									print 'done'
								end
								else
									begin
										print 'there is no such pharmacy or medicine'
										rollback transaction
									end
							end
						else
							begin
								print 'there is already such medicine in pharmacy'
								rollback transaction
							end
				end
			else
				begin
						if not exists (SELECT [medicine_id], [pharmacy_id], COUNT(*) FROM pharmacy_medicine GROUP BY [medicine_id], [pharmacy_id] HAVING COUNT(*) > 1)
							begin
								if (not exists (select inserted.medicine_id from inserted except select id from medicine) and not exists (select inserted.pharmacy_id from inserted except select id from pharmacy))
								begin
									print 'done'
								end
								else
									begin
										print 'there is no such pharmacy or medicine'
										rollback transaction
									end
							end
						else
							begin
								print 'there is already such medicine in pharmacy'
								rollback transaction
							end
				end

		end

end
go


if object_id ('post_trigger1') is not null
begin
drop trigger post_trigger1
end
go

create trigger post_trigger1 --(with cascade update)
on post
for insert, update
as
begin
	if (@@ROWCOUNT = 0)
		print 'there are 0 rows in update or insert'
	else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(*) from post inner join inserted on post.post = inserted.post) as int)<=cast((select count(*) from inserted)as int))
							begin
								print 'done'
							end
						else
							begin
								print 'there is already such post'
								rollback transaction
							end
				end
			else
				begin
						if not exists (SELECT [post], COUNT(*) FROM post GROUP BY [post] HAVING COUNT(*) > 1)
							begin
									declare post_cur1 cursor for 
									select post from deleted
									open post_cur1

									declare post_cur2 cursor for 
									select post from inserted
									open post_cur2

									declare @iddel VARCHAR(25)
									declare @idins VARCHAR(25)
									fetch next from post_cur1 into @iddel
									fetch next from post_cur2 into @idins
									while @@FETCH_STATUS = 0
										begin
											update employee set post = @idins where post = @iddel
											fetch next from post_cur1 into @iddel
											fetch next from post_cur2 into @idins
									end
									close post_cur1
									close post_cur2
									deallocate post_cur1
									deallocate post_cur2
							end
						else
							begin
								print 'there is already such post'
								rollback transaction
							end
				end
		end
end
go

if OBJECT_ID ('street_trigger1') is not null
begin
drop trigger street_trigger1
end
go

create trigger street_trigger1 --(with cascade update)
on street
for insert, update
as
begin
	if(@@ROWCOUNT = 0)
		print 'there are 0 rows to insert or update'
	else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(*) from street inner join inserted on street.street = inserted.street) as int)<=cast((select count(*) from inserted)as int))
							begin
								print 'done'
							end
						else
							begin
								print 'there is already such street'
								rollback transaction
							end
				end
			else
				begin
						if not exists (SELECT street, COUNT(*) FROM street GROUP BY street HAVING COUNT(*) > 1)
							begin
								declare street_cur1 cursor for 
									select street from deleted
									open street_cur1

									declare street_cur2 cursor for 
									select street from inserted
									open street_cur2

									declare @iddel VARCHAR(25)
									declare @idins VARCHAR(25)
									fetch next from street_cur1 into @iddel
									fetch next from street_cur2 into @idins
									while @@FETCH_STATUS = 0
										begin
											update pharmacy set street = @idins where street = @iddel
											fetch next from street_cur1 into @iddel
											fetch next from street_cur2 into @idins
									end
									close street_cur1
									close street_cur2
									deallocate street_cur1
									deallocate street_cur2
							end
						else
							begin
								print 'there is already such street'
								rollback transaction
							end
				end
		end

end
go

if object_id ('zone_trigger1') is not null
begin
drop trigger zone_trigger1
end
go

create trigger zone_trigger1 --(with cascade update)
on [zone]
for insert, update
as
begin
	if(@@ROWCOUNT = 0)
		print 'there are 0 rows to insert or update'
	else
		begin
		declare @delete int
		set @delete = cast((select count(*) from deleted) as int)
			if @delete = 0
				begin
						if (cast((select count(*) from [zone] inner join inserted on [zone].[name] = inserted.[name]) as int)<=cast((select count(*) from inserted)as int))
							begin
								print 'done'
							end
						else
							begin
								print 'there is already such zone'
								rollback transaction
							end
				end
			else
				begin
						if not exists (SELECT [name], COUNT(*) FROM [zone] GROUP BY [name] HAVING COUNT(*) > 1)
							begin
									declare zone_cur1 cursor for 
									select id from deleted
									open zone_cur1

									declare zone_cur2 cursor for 
									select id from inserted
									open zone_cur2

									declare @iddel VARCHAR(25)
									declare @idins VARCHAR(25)
									fetch next from zone_cur1 into @iddel
									fetch next from zone_cur2 into @idins
									while @@FETCH_STATUS = 0
										begin
											update medicine_zone set zone_id = @idins where zone_id = @iddel
											fetch next from zone_cur1 into @iddel
											fetch next from zone_cur2 into @idins
									end
									close zone_cur1
									close zone_cur2
									deallocate zone_cur1
									deallocate zone_cur2
							end
						else
							begin
								print 'there is already such zone'
								rollback transaction
							end
				end
		end

end
go


if object_id ('post_trigger2') is not null
begin
drop trigger post_trigger2
end
go

create trigger post_trigger2 --(restrict trigger on delete)
on post
for delete
as
begin
if exists (select post from employee where post in (select post from deleted))
begin
	print 'you are trying to delete primary key which contains foreign key in employee.post'
	rollback transaction
end
end
go

if object_id ('street_trigger2') is not null
begin
drop trigger street_trigger2
end
go

create trigger street_trigger2 --(set null trigger on delete)
on street
for delete
as
begin
	print 'done'
	update pharmacy set street = null where street in (select street from deleted)
end
go

if object_id ('zone_trigger2') is not null
begin
drop trigger zone_trigger2
end
go

create trigger zone_trigger2 --(cascade delete)
on [zone]
for delete
as
begin
	print 'done'
	delete from medicine_zone where zone_id in (select id from deleted)
end
go


--trigger that forbids any modification in post
if object_id ('post_trigger3') is not null
begin
drop trigger post_trigger3
end
go

create trigger post_trigger3
on post
instead of insert, update, delete
as
begin
	if (@@ROWCOUNT = 0)
		print 'there are 0 rows to update/insert/delete'
	else
	begin
		print 'you can not modify this table'
	end
end
go

