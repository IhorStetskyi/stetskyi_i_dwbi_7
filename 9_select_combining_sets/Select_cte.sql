use labor_sql
go

--1
;with my_cte (g_id, g_name, g_region_id)
as
(select * from [geography])

select * from my_cte
go

--2
;with my_cte1 (g_id, g_name, g_region_id)
as
(select * from [geography] where id <= 5),

my_cte2 (g_id, g_name, g_region_id)
as
(select * from my_cte1)

select * from my_cte2
go

--3
;with mycte (region_id, place_id, [name], PlaceLevel)
 as
(
select region_id, id, [name], 0 from [geography]
where region_id is null
union all
select g.region_id, g.id, g.[name], m.PlaceLevel+1 from [geography] g
inner join mycte m on m.place_id = g.region_id
)
select * from mycte
where PlaceLevel = 1
go

--4
;with mycte (region_id, place_id, [name], PlaceLevel)
 as
(
select region_id, id, [name], 0 from [geography]
where [name] = 'Ivano-Frankivsk'
union all
select g.region_id, g.id, g.[name], m.PlaceLevel+1 from [geography] g
inner join mycte m on m.place_id = g.region_id
)
select * from mycte
where PlaceLevel > 0
go

--5
;with cte (mynumber) as
(
select 1
union all
select mynumber+1 from cte where mynumber < 10000
) 
select * from cte option (maxrecursion 0)
go

--6
;with cte (mynumber) as
(
select 1
union all
select mynumber+1 from cte where mynumber < 100000
) 
select * from cte option (maxrecursion 0)
go

--7
DECLARE @StartDate DATETIME,
        @EndDate DATETIME
set @StartDate = '2018-01-01T00:00:00.000'
set @EndDate = '2018-12-31T00:00:00.000'
;with cte (day_numb,cur_date,day_name)
as
(
select DATENAME(DAYOFYEAR,@StartDate), @StartDate, DATENAME(WEEKDAY, DATEADD(day, 1, @StartDate))
union all
select DATENAME(DAYOFYEAR, DATEADD(day, 1, a.cur_date)), DATEADD(DAY, 1, a.cur_date), DATENAME(WEEKDAY, DATEADD(day, 1, a.cur_date))  from cte a where cur_date < @EndDate
)
select day_name, COUNT(day_name) as [amount] from cte 
where day_name in ('Saturday', 'Sunday')
group by day_name
option (maxrecursion 0)
go

--8
select distinct [maker] from product 
where [type] = 'pc' 
and 
maker not in (select distinct [maker] from product where [type] = 'laptop')
go

--9
select distinct [maker] from product
where [type] = 'pc'
and 
maker != all (select distinct [maker] from product where [type] = 'laptop')
go

--10
 select distinct maker from product
 where type = 'pc'
 and not
 maker = any (select distinct maker from product where [type] = 'laptop')
 go

 --11
select distinct [maker] from product 
where [type] = 'pc' 
and 
maker in (select distinct [maker] from product where [type] = 'laptop')
go

--12
select distinct [maker] from product
where [type] = 'pc'
and not
maker != all (select distinct [maker] from product where [type] = 'laptop')
go

--13
 select distinct maker from product
 where type = 'pc'
 and
 maker = any (select distinct maker from product where [type] = 'laptop')
 go

 --14
select distinct maker from product where type = 'pc'
and maker not in
(
select distinct maker from product where type = 'pc'
and
model != all (select model from pc)
)
go

--15
if exists (select top 1 * from classes where country = 'Ukraine')
select country, class from classes where country = 'Ukraine'
else
select country, class from classes
order by country

--16
select ship, battle, [date] from outcomes
inner join battles on battles.[name] = outcomes.battle
where ship in
(
select ship as battles_amount from outcomes
group by ship
having count(ship) > 1
)
--or
select o1.ship, o1.battle, b1.[date] from outcomes o1
inner join battles b1 on b1.[name] = o1.battle
inner join (select ship, battle, [date] from outcomes o2
			inner join battles b2 on b2.[name] = o2.battle) a on a.ship = o1.ship and a.[date] > b1.[date]
go

--17
select distinct maker from product where [type] = 'pc' and maker not in
(
select distinct maker from product where product.[type] = 'pc' and not exists(select * from pc where pc.model = product.model)
)
go

--18
;with my_cte
as
(
select maker, max(speed) as speed from pc
inner join product on pc.model = product.model
where maker in
(
select distinct maker from product where [type] = 'printer'
and maker in (select distinct maker from product where [type] = 'pc')
)
group by maker
)

select maker from my_cte
where speed = (select MAX(speed) from my_cte)
--or
select maker from product
inner join pc on product.model = pc.model
where speed =
(
select max(speed) as speed from pc
inner join product on pc.model = product.model
where maker in
(
select distinct maker from product where [type] = 'printer'
and maker in (select distinct maker from product where [type] = 'pc')
)
)
go

--19
select class from ships where 
[name] in (select ship from outcomes 
			where result = 'sunk')
union
select class from classes where
class in (select ship from outcomes 
			where result = 'sunk')
go

--20
;with cte
as
(
select model, MAX(price) as price from printer
group by model
)
select model, price from cte
where price = (select max(price) from cte)
go

--21
select product.[type], laptop.model, speed from laptop
inner join product on product.model = laptop.model
where speed < all (select speed from pc)

--22
;with cte as
(
select product.maker, printer.price from printer
inner join product on product.model = printer.model
where color = 'y'
)

select * from cte
where price = (select min(price) from cte)
go

--23
select o.battle, c.country, count(o.ship) as ship_count from outcomes o
left join ships s on s.[name] = o.ship
left join classes c on o.ship = c.class or s.class = c.class
where c.country is not null
group by c.country, o.battle
having count(o.ship) >= 2
go

--24
select product.maker, count(pc.model) as pc, count(laptop.model) as laptop, count(printer.model) as printer from product
left join pc on pc.model = product.model
left join laptop on laptop.model = product.model
left join printer on printer.model = product.model
group by product.maker
go

--25
select maker,
	case 
		when [count]=0 then 'no'
		else 'yes('+cast ([count] as varchar(10))+')'
	end 
[count] from 
(
select maker, cast(count(pc.model) as varchar(10)) as [count] from product
left join pc on product.model = pc. model
group by maker
) a
go

--26
select income_o.point, income_o.[date], inc as income, [out] as outcome from income_o
inner join outcome_o on outcome_o.point = income_o.point and income_o.[date] = outcome_o.[date]
go

--27
select s.[name], numGuns, bore, displacement, [type], country, launched, c.class from ships s
inner join classes c on s.class = c.class
where
case when numGuns = 8 then 1 else 0 end +
case when bore = 15 then 1 else 0 end +
case when displacement = 32000 then 1 else 0 end +
case when [type] = 'bb' then 1 else 0 end +
case when country = 'USA' then 1 else 0 end+
case when launched = 1915 then 1 else 0 end +
case when s.class = 'Kongo' then 1 else 0 end 
>= 4
go

--28
;with point_date_cte
as
(
  select point, [date]
  from outcome_o
  union
  select point, [date]
  from outcome
),

 outcome_cte
 as
 (
   select point, [date], sum([out]) as [out]
   from outcome
   group by point, [date]
 ),
 out_cte
 as
 (
  select pdc.point, pdc.[date], coalesce(oo.[out],0) as once_day_out, coalesce(oc.[out],0) as more_once_out
  from point_date_cte pdc
  left join outcome_o as oo
  on pdc.point=oo.point and pdc.[date]=oo.[date]
  left join outcome_cte as oc
  on pdc.point=oc.point and pdc.[date]=oc.[date]
 )

 select point, [date],
        case
		 when once_day_out>more_once_out then 'once a day'
		 when once_day_out<more_once_out then 'more than once a day'
		 else 'both'
		end as lider
 from out_cte;
go

--29
select maker, product.model, product.[type], coalesce(pc.price, laptop.price, printer.price) as price from product
left join pc on pc.model = product.model
left join laptop on laptop.model = product.model
left join printer on printer.model = product.model
where maker = 'B'
go

--30
select [name], [name] as class from
(select [name] from ships
union
select ship from outcomes) a
inner join classes on a.[name] = classes.class
go

--31
select class as [count] from
(
select class  from ships
union all
(
select class from outcomes
inner join classes on classes.class = outcomes.ship
)
) a
group by class
having count(class) = 1
go

--32
select [name] from ships where launched < 1942
union
select ship from outcomes
inner join battles on battles.[name] = outcomes.battle and DATEPART(year, battles.[date]) < 1942
go
