use labor_sql
go

--1
select row_number()over(order by id_comp,trip_no) num, trip_no, id_comp from trip
go

--2
select row_number()over(partition by id_comp order by id_comp,trip_no) num, trip_no, id_comp from trip
go

--3
select model, color, [type], price from 
(select model, color, [type], price, min(price)over(partition by [type]) as min_price from printer) p
where p.price = p.min_price
go

--4
select maker from product where [type] = 'pc'
group by maker
having count(model) > 2
--or
select distinct maker from
(
select *, count(maker)over(partition by maker) as [count] from product where [type] = 'pc'
) a where count > 2
go

--5
;with cte as
(
select dense_rank()over(order by price desc) as 'SecondMaximum',* from pc
)

select price from cte where SecondMaximum=2;
go

--6
;with cte as
(
select *, SUBSTRING([name], charindex(' ', [name])+1, 100) as [Surname] from passenger
)

select *, ntile(3)over(order by (surname)) as [group] from cte
go

--7
declare @a int = (select COUNT(*)%3 from pc)
declare @page_total int = 
case
when @a = 0 then (select COUNT(*)/3 from pc)
when @a > 0 then (select COUNT(*)/3 from pc)+1
end
select code, model,ram,hd,cd,price,id,count(*)over(partition by every) total, page_num, @page_total as page_total from
(
select *, row_number()over(order by price desc) id
		,NTILE(@page_total) over(order by price desc) page_num
		,1 as every
from pc
) a 
go

--8
;with cte as 
(
	select point,[date],[out] as [money], 'out' as [type] from outcome
	union all
	select point,[date],[out] as [money], 'out' as [type] from outcome_o
	union all 
	select point,[date],inc as [money], 'inc' as [type] from income
	union all 
	select point,[date],inc as [money], 'inc' as [type] from income_o
)
select [money] as [max_sum],[type],[date],[point]
from    cte
where   [money] = (select MAX([money]) from cte)
go

--9
select *, 
price - avg(price)over(partition by speed) as dif_local_price, 
price - avg(price)over() as dif_local_price,
avg(price)over() as total_price from pc
go